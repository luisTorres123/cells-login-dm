import { LitElement, html, } from 'lit-element';

import '@bbva-web-components/bbva-core-generic-dp/bbva-core-generic-dp.js';
/**
@customElement cells-login-dm
*/
export class CellsLoginDm extends LitElement {
  static get is() {
    return 'cells-login-dm';
  }

  // Declare properties
  static get properties() {
    return {
      documentType: { type: String },
      documentNumber: { type: String },
      pwd: { type: String },
    };
  }

  // Initialize properties
  constructor() {
    super();
  }

  // Define a template
  render() {
    return html`
      <slot></slot>
      <bbva-core-generic-dp
        id="loginDP"
        host="https://tst-glomo.bbva.pe/SRVS_A02/"
        path="TechArchitecture/pe/grantingTicket/V02"
        method="POST"
        body='{
          "authentication": {
            "userID": "${this.documentType.concat(this.documentNumber)}",
            "consumerID": "13000013",
            "authenticationType": "02",
            "authenticationData": [
              {
                "idAuthenticationData": "password",
                "authenticationData": [
                  "${this.pwd}"
                ]
              }
            ]
          },
          "backendUserRequest": {
            "userId": "",
            "accessCode": "${this.documentType.concat(this.documentNumber)}",
            "dialogId": ""
          }
        }'
        @request-success="${this._loginSuccess}" 
        @request-error="${this._loginError}">
      </bbva-core-generic-dp>
    `;
  }

  authentication() {
    let dp = this.shadowRoot.getElementById('loginDP');
    dp.generateRequest();
  }

  _loginSuccess(ev) {
    this.dispatchEvent(new CustomEvent('login-success', {
        bubbles: true,
        composed: true,
        detail: ev.detail
      })
    );
  }

  _loginError(ev) {
    this.dispatchEvent(new CustomEvent('login-error', {
        bubbles: true,
        composed: true,
        detail: ev.detail
      })
    );
  }
}
